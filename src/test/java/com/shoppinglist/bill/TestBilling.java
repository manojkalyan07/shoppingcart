package com.shoppinglist.bill;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class TestBilling {
	Billing bill;
	@Before
	public void intialize() {
		bill= new Billing();
	}
	@Test
	public void test() {
		Double value=27500.0;
		Double counter = bill.counter("Products.json", "Category.json",bill);
		
		Assert.assertEquals(value, counter);
		
	}
	@Test
	public void test2() {
		Double value=124400.0;
		Double counter = bill.counter("Products2.json", "Category.json",bill);
		
		Assert.assertEquals(value, counter);
		
	}
	@Test
	public void test3() {
		Double value=125025.923;
		Double counter = bill.counter("Products2.json", "Category2.json",bill);
		
		Assert.assertEquals(value, counter);
		
	}
	@Test
	public void test4() {
		Double value=67536.387;
		Double counter = bill.counter("Products3.json", "Category2.json",bill);
		
		Assert.assertEquals(value, counter);
		
	}
	

}
