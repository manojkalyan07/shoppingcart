package com.shoppinglist.bill.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Items {
	
	@JsonProperty("name")
	private String name;
	@JsonProperty("price")
	@JsonInclude(Include.NON_NULL)
	private int price;
	@JsonProperty("quantity")
	private int quantity;
	
	@JsonProperty("category")
	private String category;
	
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Items [name=" + name + ", price=" + price + ", quantity=" + quantity + ", category=" + category + "]";
	}

	

}
