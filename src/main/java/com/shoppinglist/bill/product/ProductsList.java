package com.shoppinglist.bill.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductsList {
	
	@JsonProperty("items")
	private List<Items> products;

	public List<Items> getItems() {
		return products;
	}

	public void setItems(List<Items> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "ProductsList [products=" + products + "]";
	}

}
