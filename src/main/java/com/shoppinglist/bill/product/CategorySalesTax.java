package com.shoppinglist.bill.product;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategorySalesTax {

	@JsonProperty("a")
	private Double categoryA;
	@JsonProperty("b")
	private Double categoryB;
	@JsonProperty("c")
	private Double categoryC;
	
	

	public Double getCategoryA() {
		return categoryA;
	}

	public void setCategoryA(Double categoryA) {
		this.categoryA = categoryA;
	}

	public Double getCategoryB() {
		return categoryB;
	}

	public void setCategoryB(Double categoryB) {
		this.categoryB = categoryB;
	}

	public Double getCategoryC() {
		return categoryC;
	}

	public void setCategoryC(Double categoryC) {
		this.categoryC = categoryC;
	}

	}
