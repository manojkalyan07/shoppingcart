package com.shoppinglist.bill.exception;

public class PriceNotavailableException extends Exception {

	
	public PriceNotavailableException(String message){
		
		super(message);
	}
}
