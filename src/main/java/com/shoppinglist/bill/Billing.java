package com.shoppinglist.bill;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shoppinglist.bill.exception.PriceNotavailableException;
import com.shoppinglist.bill.product.CategorySalesTax;
import com.shoppinglist.bill.product.Items;
import com.shoppinglist.bill.product.ProductsList;

public class Billing {



	public static void main(String args[]) {

		Billing billing=new Billing();
		billing.counter("Products.json", "Category.json",billing);
	}

	public Double counter(String fileName,String fileName2,Billing billing) {

		ProductsList productslist = null;
		CategorySalesTax categorysalestax = null;
		ObjectMapper objectMapper = new ObjectMapper();

		//Getting file details
		File productData = getFile(fileName);

		File categoryData = getFile(fileName2);

		//Mapping Json to Pojo
		try {
			productslist = objectMapper.readValue(productData, ProductsList.class);
		} catch (Exception e) {

			e.printStackTrace();
		}
		try {
			categorysalestax = objectMapper.readValue(categoryData, CategorySalesTax.class);
		} catch (Exception e) {

			e.printStackTrace();
		}

		//Started Billing
		return billing.startBilling(productslist,categorysalestax);

	}

	public double startBilling(ProductsList productslist,CategorySalesTax categorysalestax ) 
	{

		List<Items> items = productslist.getItems();

		int sum = items.stream().mapToInt(item->
		{	
			int price = item.getPrice();

			try {
				if(price==0) {
					throw new PriceNotavailableException("Price not generated for "+item.getName());
				}
				
				int calculatedprice=item.getPrice()*item.getQuantity();
				System.out.println("Price of "+item.getName()+ " is :"+calculatedprice);
				return calculatedprice;


			} catch (PriceNotavailableException e) {

				System.out.println(e.getMessage());
				
				return 0;
			}

			/*if(price==0) {


					try {
						throw new PriceNotavailableException("Price not generated");
					} catch (PriceNotavailableException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


			}
			else {
				System.out.println("Price of "+item.getName()+ " is :"+price);

				return price;
			}
			return price;*/





		}).sum();

		double taxSum = items.stream().mapToDouble(item->
		{	
			int totalPrice = item.getPrice()*item.getQuantity();

			Double taxAmount = null;
			switch(item.getCategory()) 
			{

			case "a":	

				Double a=categorysalestax.getCategoryA();
				taxAmount= (double) (totalPrice*(a/100));
				System.out.println("Tax on "+item.getName()+ " is :"+taxAmount);
				break;

			case "b":	

				Double b=categorysalestax.getCategoryB();
				taxAmount= (double) (totalPrice*(b/100));
				System.out.println("Tax on "+item.getName()+ " is :"+taxAmount);
				break;

			case "c":	

				Double c=categorysalestax.getCategoryC();
				taxAmount= (double) (totalPrice*(c/100));
				System.out.println("Tax on "+item.getName()+ " is :"+taxAmount);
				break;
			}

			return taxAmount;

		}).sum();

		System.out.println("--------------------------------------------------------------");

		System.out.println("Total cost of all products : "+sum);

		System.out.println("Total tax of all products  : "+taxSum);

		System.out.println("--------------------------------------------------------------");

		System.out.println("Total Cost of all products including taxes : "+(sum+taxSum));
		return (sum+taxSum);
	}


	public File getFile(String fileName) {

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		return file;
	}


}
